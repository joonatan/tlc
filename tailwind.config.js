// tailwind.config.js
module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  content: [
    './src/**/*.php',
    './src/**/*.js'
  ],
  theme: {
    fontFamily: {
      display: ['Open Sans', 'sans-serif'],
      'ma-icons': ['Material Icons'],
      body: ['Open Sans', 'sans-serif'],
    },
    extend: {
      colors: {
        'tuni-violet': '#4e008e',
        'tuni-light': '#c3b9d7',
        'tuni-gray': '#c8c8c8',
        'tuni-pink': '#f0667b',
      },
      zIndex: {
        'bottom': '-999',
        'top': '999',
        'nav': '100',
        '100': '100',
        'burger': '110',
      },
      height: {
        'menu': 'min(80%, 50rem)',
      },
      minHeight: {
        '220': '55rem',
      },
      maxHeight: {
        '128': '32rem',
      },
      width: {
        '80': '20rem',
        'menu': 'min(90%, 40rem)',
        '80p': '80%',
      },
      maxWidth: {
        'widget': '300px',
        'blog-card': '38rem',
        'screen-md': '768px',
        'screen-lg': '1024px',
        'screen-xl': '1280px',
        'screen-2xl': '1536px',
      },
      minWidth: {
        'widget': '300px',
        'sub-menu': '28rem',
        '120': '30rem',
        '150': '35rem',
      },
      gridTemplateColumns: {
        auto: 'repeat(auto-fill, minmax(30rem, 1fr))',
        'auto-sm': 'repeat(auto-fill, minmax(24rem, 1fr))',
      },
      inset: {
        '40': '10rem',
        '24': '6rem',
        '20': '5rem',
        '8': '2rem',
        '4': '1rem',
      },
      listStyleType: {
        'circle': 'circle',
      },
      lineHeight: {
        '12': '3rem',
      },
      margin: {
        '60': '15rem',
        '-60': '-15rem',
        '-48': '-12rem',
        '-40': '-10rem',
        '-32': '-8rem',
        '-20': '-5rem',
      },
      borderRadius: {
        sm: '0.25rem',
        md: '0.5rem',
        lg: '0.8rem',
        xl: '1rem',
      },
    },
    screens: {
      'sm': '500px',
      'md': '768px',
      'lg': '950px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    fontSize: {
      'xs': '1.3rem',
      'sm': '1.5rem',
      'nav-sm': '1.6rem',
      'md': '1.8rem',
      'base': '1.8rem',
      '20': '2rem',
      'lg': '2.2rem',
      'xl': '2.5rem',
      '2xl': '3rem',
      '3xl': '3.5rem',
      'massive': '20rem',
    },
    boxShadow: {
      default: '0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)',
      'tuni-sm': '2px 2px 2px 0px rgba(78, 0, 142, .3), 2px 2px 0px 0px rgba(78, 0, 142, .2)',
      sm: '2px 2px 2px 0px rgba(0, 0, 0, .1), 2px 2px 0px 0px rgba(0, 0, 0, .06)',
      md: '0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)',
      lg: '2px 2px 4px 2px rgb(0 0 0 / 40%), 2px 2px 4px 0px rgb(0 0 0 / 5%)',
      xl: '0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)',
      '2xl': '0 25px 50px -12px rgba(0, 0, 0, .25)',
      '3xl': '0 35px 60px -15px rgba(0, 0, 0, .3)',
      inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
      outline: '0 0 0 5px #4e008e',
      focus: '0 0 0 5px rgba(0, 0, 225, 0.5)',
      'none': 'none',
    }
  }
}
