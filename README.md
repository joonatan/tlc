# WP theme for TLC website

WordPress theme for Tampere University Teaching and Learning Centre [TLC](https://www.tuni.fi/tlc/) website. 

## Needed plugins

[Algolia search plugin](https://gitlab.com/joonatan/wp-algolia)

[Event calendar](https://gitlab.com/joonatan/tlc-intra-events)

[Custom block component](https://gitlab.com/joonatan/tlc-blocks)

## Required tooling

WordPress: 6.5+ (tested on 6.6 and 6.7)
php: 8.0+ (tested on 8.0.30)

Javascript build system for TailwindCSS and bundling tools.

- yarn
- gulp
- talwindcss

Docker for local development environment.

## Setup

Bring the WP server and MariaDB up.
```sh
docker compose up -d
```

Data transfers are assumed to be manual from production to local. Use the Duplicator plugin.

Source code modifications are transfered to docker container manually with `gulp`
```sh
gulp
```

This runs both the build step (tailwindcss) and copies all `.php` files in the source directory.

Bundle a package for distribution
```sh
gulp zip
```

Same as copy to local docker environment but makes a `zip` of the output files instead.

Update translation files
```sh
gulp pot
```

Translation files require updates rarely. If you introduce new custom translation keys in the theme,
you should run the `pot` command and update the `.po` translation files, and compile them to `.mo` files before distribution.
