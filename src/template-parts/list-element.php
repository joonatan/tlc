<?php
$h_elem = get_query_var('header_elem', 'h3');
?>
<!-- For search:
      - showing the keywords to user is important
      - the user doesn't know where the page is so show the page type
-->
<!-- @todo fix the feature image height (max height is few pixels too short compared to the text) -->
<!-- use padding instead of margin otherwise w-full creates weird margin on right side -->
<div class="py-4 px-2 max-w-screen-lg">
<div class="card-section border-2 border-tuni-light w-full flex flex-col lg:flex-row p-0 m-0 relative"
   >
  <a class="flex flex-col md:flex-row"
     href="<?php echo esc_url( get_permalink() ); ?>">

    <div class="w-full h-auto md:h-80 md:w-80 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
         title="<?php the_title(); ?>">
      <div class="hidden md:block">
        <?php the_post_thumbnail('list-thumb'); ?>
      </div>
      <div class="block md:hidden">
        <?php the_post_thumbnail('grid-thumb'); ?>
      </div>
    </div>

    <div class="border-gray-400 lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
      <!-- @todo how to set the header element -->
      <<?= $h_elem; ?> class="text-gray-900 text-lg font-bold p-0 mb-2"><?php the_title(); ?><<?= '/' . $h_elem; ?>>
      <div class="text-gray-700"><?php the_excerpt(); ?></div>
      <div class="flex flex-row">
        <div class="text-md text-gray-800 m-0"><?php the_time('Y-m-d'); ?></div>
        <?php if ( is_sticky() ): ?>
          <div class="ml-auto text-tuni-violet bg-tuni-light border-tuni-violet border-2 rounded-md p-2">STICKY</div>
        <?php endif; ?>
      </div>
    </div>

    <!-- What type of a page is it: blog post, project, page, event -->
    <div class="post-type-note">
      <?php get_template_part( 'template-parts/post-type' ); ?>
    </div>
  </a>

  <!-- push the last segment to the right side -->
  <div class="mx-auto"></div>

  <!-- Meta data about the post: on the right on large screens -->
  <div class="flex flex-col">
    <!-- categories for blog posts -->
    <?php
    $cats = array_filter( wp_get_post_categories( get_the_ID() ), function ($x) {
      $cat = get_category($x);
      if (strtolower($cat->name) != 'uncategorized') {
        return true;
      } else {
        return false;
      }
    });
    ?>

    <?php if ( !empty($cats ) ): ?>
    <div class="w-full h-auto md:w-64 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden my-auto">
      <div class="flex flex-col md:flex-row lg:flex-col">
      <?php foreach ( $cats as $c ):
        $cat = get_category( $c );
        ?>
        <a class="btn-sm my-4 mx-auto md:mx-4"
           href="<?php echo esc_url( get_category_link( $cat->term_id ) ) ?>"
           >
           <?php echo $cat->name; ?>
        </a>
      <?php endforeach; ?>
      </div>
    </div>
    <?php endif; ?>

    <!-- Keywords when showing search results -->
    <?php if ( is_search() ): ?>
      <?php $keys = wp_get_post_terms( get_the_ID(), 'keywords', array( 'fields' => 'names' ) ); ?>
      <div class="flex-grow flex flex-col m-4">
        <?php if (!empty($keys)): ?>
        <h6 class="text-tuni-violet"><?php _e('Keywords'); ?></h6>
        <?php
        foreach ($keys as $key) {
          echo '<div class="text-sm">' . $key . '</div>';
        } ?>
        <?php endif; // keys ?>
      </div>
    <?php endif; // is_search ?>
  </div>
</div>
</div>
