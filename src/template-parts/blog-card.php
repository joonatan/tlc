<?php $h_elem = get_query_var('header_elem', 'h3'); ?>

<!-- need zero margin and padding so that the percentage width works properly -->
  <a
     class="card-section flex flex-col shadow-sm bg-violet-50 rounded-lg h-full pb-8 relative outline-none hover:shadow-lg focus-within:shadow-lg"
     href="<?php echo esc_url( get_permalink() ); ?>"
  >
      <?php // @todo need a custom thumbnail size or smaller cards for 300x300 (medium) ?>
      <?php the_post_thumbnail('grid-thumb') ?>
      <div class="card-text mt-6 mx-10">
        <<?= $h_elem; ?>  class="text-opacity-100 text-md text-slate-700 font-medium">
           <?php the_title(); ?>
        <<?= '/' . $h_elem; ?>>

        <div class="flex flex-row">
          <?php if ( is_sticky() ): ?>
            <div class="ml-auto text-tuni-violet bg-tuni-light border-tuni-violet border rounded-md p-2">STICKY</div>
          <?php endif; ?>
        </div>
      </div>

      <div class="absolute right-0 top-0 text-sm text-slate-700 border-l border-b border-violet-400 rounded-bl-lg bg-violet-100 m-0 p-2">
        <?php if(pll_current_language() === 'en'): ?>
          <?php the_time('j M Y'); ?>
        <?php else: ?>
          <?php the_time('j. M Y'); ?>
        <?php endif; ?>
      </div>

      <!-- What type of a page is it: blog post, project, page, event -->
      <div class="post-type-note">
        <?php get_template_part( 'template-parts/post-type' ); ?>
      </div>
    </a>

