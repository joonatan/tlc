<?php
$logo_override = get_query_var('logo_override');
$logo = $logo_override ? $logo_override : get_theme_mod( 'tlc_theme_logo' );
?>
<a class="flex items-center flex-shrink-0 text-black mx-auto pt-4 pb-8 logo-link " href="<?php bloginfo('url'); ?>">
  <img class="w-full h-40 "
       src="<?php echo $logo; ?>"
       alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) . ' ' . __('Homepage') ); ?>" />
</a>
