<?php
/**
 * Template for displaying single page content
 *
 * Part of TLC theme
 */

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <header class="entry-header">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  </header><!-- .entry-header -->

  <?php if ( has_post_thumbnail() && ! post_password_required() ): ?>
    <figure class="featured-media md:pr-8 pb-4">
      <?php
      the_post_thumbnail();

      $caption = get_the_post_thumbnail_caption();

      if ( $caption ):
        ?>
        <figcaption class="wp-caption-text mx-8"><?= esc_html( $caption ); ?></figcaption>
      <?php endif; ?>
    </figure><!-- .featured-media -->
  <?php endif; ?>

  <?php if (get_post_type() == 'post' || get_post_type() == 'news' ): ?>
    <div class="mb-8 italic text-sm">
      <?php if(pll_current_language() === 'en'): ?>
        <?php the_time('j F Y'); ?>
      <?php else: ?>
        <?php the_time('j. F Y'); ?>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <div class="entry-content">
    <?php the_content( __( 'Continue reading', 'twentytwenty' ) ); ?>
  </div><!-- .entry-content -->

  <!-- add edit section -->
  <?php if ( is_user_logged_in() && !is_front_page() ) : ?>
  <div class="py-4 flex flex-row w-full justify-around">
    <?php edit_post_link( __( 'Edit', 'tlc-custom' ), '<div class="btn-sec text-center">', '</div>', null, ''); ?>
  </div>
  <?php endif; ?>

  <?php
  // any post type other than page
  if ( is_single() ) {
    get_template_part( 'template-parts/navigation' );
  }
  ?>
</article><!-- .post -->

