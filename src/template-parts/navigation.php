<?php
/**
 * Displays the next and previous post navigation in single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$next_post = get_next_post();
$prev_post = get_previous_post();

$pagination_classes = '';

if ( ! $next_post ) {
  $pagination_classes = ' only-one only-prev';
} elseif ( ! $prev_post ) {
  $pagination_classes = ' only-one only-next';
}

?>
<nav class="flex justify-between lg:mx-10 my-4 <?= esc_attr( $pagination_classes ); ?>"
     aria-label="<?php esc_attr_e( 'Post', 'twentytwenty' ); ?>"
     role="navigation">
  <?php if ( $prev_post ): ?>
    <div class="btn-sec-sm">
      <a
        href="<?= esc_url( get_permalink( $prev_post->ID ) ); ?>"
        title="<?= wp_kses_post( get_the_title( $prev_post->ID ) ); ?>"
      >
        <span class="arrow" aria-hidden="true">&larr;</span>
        <span><?= _e('Previous'); ?></span>
      </a>
    </div>
  <?php else: ?>
    <div></div>
  <?php endif; ?>

  <?php $ref = get_post_type_archive_link( get_post_type() ); ?>
  <div class="btn-sec-sm">
    <a href="<?= esc_url($ref); ?>">
      <span class="arrow" aria-hidden="true">&uarr;</span>
      <?php _e('All', 'tlc-custom'); ?>
    </a>
  </div>

  <?php if ( $next_post ): ?>
    <div class="btn-sec-sm">
      <a
        href="<?= esc_url( get_permalink( $next_post->ID ) ); ?>"
        title="<?php echo wp_kses_post( get_the_title( $next_post->ID ) ); ?>"
      >
        <span><?= _e('Next'); ?></span>
        <span class="arrow" aria-hidden="true">&rarr;</span>
      </a>
    </div>
  <?php else: ?>
    <div></div>
  <?php endif; ?>
</nav><!-- .pagination-single -->

