<?php
// TODO this should really be a function not a template part
// since it returns text not html content
// TODO this should respect translation rules
?>

<?php
$type = get_post_type();
switch ( $type ) {
  case 'post':
    _e( 'Blog', 'tlc-custom' );
    break;
  case 'page':
    _e( 'Page', 'tlc-custom' );
    break;
  case 'mec-events':
    _e( 'Calendar', 'tlc-custom' );
    break;
  case 'projects':
    _e( 'RDI', 'tlc-custom' );
    break;
  case 'news':
    _e( 'News', 'tlc-custom' );
    break;
  default:
    _e( $type, 'tlc-custom' );
    break;
}

?>
