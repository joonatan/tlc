<?php
/**
 */

?><!DOCTYPE html>

<!-- TODO need a search button (icon) : this should appear after the user writes something
     TODO need a clear button (X) : this should appear if there is text in it
 -->
<form role="search" method="get" class="search-form" action="<?php echo esc_url( get_site_url() ); ?>">
    <!-- @todo the message should be based on where we are searching (query vars) -->
    <input type="search" class="search-field w-full"
           placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'local search', 'tlc-custom' ) ?>"
           value="<?php echo get_search_query() ?>"
           aria-label="<?php _e('Search'); ?>"
           name="s" />
    <button type="submit" aria-label="<?php _e('Search'); ?>">
       <span class="font-ma-icons text-xl font-bold">search</span>
    </button>
</form>
