<?php
/*
 * Template Name: Extra Large Template
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <?php get_template_part( 'template-parts/layout-start' ); ?>

  <div class="hidden lg:block">
    <?php set_query_var('sidebar', 'page'); ?>
    <?php get_sidebar(); ?>
  </div>

  <main id="main" class="flex-grow content max-w-screen-xl" tabindex="-1">
    <?php get_template_part( 'template-parts/render_all_posts' ); ?>
  </main>

  <?php get_template_part( 'template-parts/layout-end' ); ?>

</html>
