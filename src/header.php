<?php
/**
 * Header for TLC theme
 *
 * Part of TLC theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >

  <link rel="profile" href="https://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

  <?php wp_head(); ?>
</head>

  <header id="site-header">
    <a class="skip-main" href="#main"><?php _e('Skip to content') ?></a>
    <div>
      <nav class="top-menu flex justify-between flex-wrap bg-tuni-violet pr-8 pb-4">
        <div class="flex justify-between items-center flex-shrink-0 text-white ml-0 lg:ml-6">
          <a class="max-w-full" target="_blank" rel="noopener nofollow" href="https://tuni.fi/">
            <img class="logo inline p-2 w-auto h-24"
                 src="<?= tlc_get_parent_logo(); ?>"
                 alt="<?php _e('Tampere Universities'); ?>">
          </a>
        </div>
        <button class="inline-block md:hidden" aria-label="<?= _e('Search'); ?>" onclick="showSearch(true)" >
          <span class="font-ma-icons text-3xl text-white">search</span>
        </button>
        <div class="search-widget hidden md:block ml-auto">
          <div class="search-widget-inner inline-block ml-auto hover:text-slate-600 mr-4 mt-2">
            <div class="searchbox-container relative">
              <div id="searchbox"></div>
              <div id="stats"></div>
            </div>
            <div class="relative">
              <div id="hits-container">
                <div id="hits"></div>
                <div id="clear-refinements"></div>
                <div id="tags-list"></div>
                <div id="pagination"></div>
              </div>
            </div>
          </div>
          <button class="inline-block md:hidden absolute top-4 right-4" aria-label="<?= _e('Close'); ?>" onclick="showSearch(false)" >
            <span class="font-ma-icons text-3xl bg-tuni-violet text-white">close</span>
          </button>
          <?php
          wp_nav_menu( array(
            'menu' => 'top-menu',
            'theme_location' => 'top-menu',
            'depth' => 0,
            'container' => false,
            'menu_class' => 'block ml-auto md:inline-block mr-4 no-underline text-nav-sm text-white',
          ));
          ?>
        </div>
      </nav>

      <!-- hamburger for mobile navigation -->
      <!-- @todo this needs to be under the top-menu in DOM
          otherwise it allows us to scroll the menu -->
      <!-- @todo translation -->
      <!-- @todo should the aria-label change based on the input state -->
      <nav class="main-menu flex flex-wrap xl:px-10 bg-white shadow-tuni-sm fixed lg:relative bottom-0 right-0 h-24 lg:h-auto w-full z-nav">
        <div class="w-1/2 h-auto lg:hidden">
          <a class="flex items-center flex-shrink-0 text-black mx-auto logo-link " href="<?php bloginfo('url'); ?>">
            <img class="w-auto h-20 px-2 pt-2"
                 src="<?php echo get_theme_mod( 'tlc_theme_logo' ); ?>"
                 alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) . ' ' . __('Homepage') ); ?>" />
          </a>
        </div>
        <input type="checkbox" id="menyAvPaa" role="button"
               class="block lg:hidden absolute top-4 right-8 text-white"
               onchange="hideOverflow(this);"
               onkeypress="checkOnEnter(this, event);"
               aria-label="<?php _e('menu', 'tlc-custom'); ?>">
        </input>
        <div id="meny" class="flex flex-col lg:flex-row lg:justify-between z-50 fixed lg:relative bottom-24 lg:bottom-0 right-0 py-8 lg:p-0 h-menu lg:h-auto w-menu lg:w-full max-w-screen-2xl m-auto">
          <?php
          wp_nav_menu( array(
            'menu' => 'header-menu',
            'theme_location' => 'header-menu',
            'depth' => 0,
            'container' => false,
            'menu_class' => 'w-full flex flex-col lg:flex-row lg:justify-between items-start lg:items-center m-0 mb-auto lg:mb-0',
            'after' => '<input type="checkbox" role="button" class="icon open-sub-menu mx-2 absolute right-4 lg:right-0" aria-label="expand"></input>'
          ));
          ?>
          <?php
          wp_nav_menu( array(
            'menu' => 'mobile-only-menu',
            'theme_location' => 'mobile-only-menu',
            'depth' => 0,
            'container' => false,
            'menu_class' => 'w-full flex flex-col lg:hidden items-start lg:items-center m-0',
            'after' => '<input type="checkbox" role="button" class="icon open-sub-menu mx-2 absolute right-4 lg:right-0" aria-label="expand"></input>'
          ));
          ?>
          <ul class="lg:hidden list-none align-self-end">
            <?php langugage_selector();?>
          </ul>
        </div>
      </nav>
    </div>
  </header>

</html>

<script>
// check mobile menu state at start
const x = document.getElementById('menyAvPaa');
if (x) {
  hideOverflow(x);
}

// highlight the top menu
const url =  window.location.href
const els = document.getElementById('meny').querySelectorAll(`a[href='${url}']`);

els.forEach((x) => {
  const b = x.parentNode
  highlightSelected(b)
  // highlight parent nodes
  highlightRecursion(b)
})

const showSearch = (show) => {
  const widget = document.querySelector('.search-widget')
  if (show) {
    widget.style.display = 'block';
    const search = document.querySelector('#searchbox .ais-SearchBox-input')
    search.select()
  } else {
    widget.style.display = 'none';
  }
}

const menuItems = document.querySelectorAll('.menu-item-has-children')
const btns = document.querySelectorAll('.open-sub-menu')
for (let i = 0; i < menuItems.length; i++) {
  const item = menuItems[i]
  item.addEventListener('mouseover', (e) => {
    setMenuDirection()
  })
}
for (let i = 0; i < btns.length; i++) {
  const btn = btns[i]
  btn.addEventListener('click', (e) => {
    setMenuDirection()
  })
}

window.addEventListener('resize', setMenuDirection)

</script>

