<?php
/**
 * Template Name: Full Width Template
 *
 * Part of TLC theme
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <?php get_template_part( 'template-parts/layout-start' ); ?>

  <div class="hidden lg:block">
    <?php set_query_var('sidebar', 'page'); ?>
    <?php get_sidebar(); ?>
  </div>

  <main id="main" class="flex-grow mx-0 content max-w-full" tabindex="-1">
    <?php get_template_part( 'template-parts/render_all_posts' ); ?>
  </main>

  <?php get_template_part( 'template-parts/layout-end' ); ?>

</html>
