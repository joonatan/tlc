<?php
/**
 * Template Name: No sidebar large
 *
 * Part of TLC theme
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <?php get_template_part( 'template-parts/layout-start' ); ?>

  <main id="main" class="flex-grow mx-0 content max-w-screen-xl" tabindex="-1">
    <?php get_template_part( 'template-parts/render_all_posts' ); ?>
  </main>

  <?php get_template_part( 'template-parts/layout-end' ); ?>

</html>

