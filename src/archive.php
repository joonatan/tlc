<?php
/**
 * Template Name: Archives
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <?php get_template_part( 'template-parts/layout-start' ); ?>

  <?php if (get_post_type() != 'news'): ?>
    <div class="hidden lg:block">
      <?php set_query_var('sidebar', 'blog'); ?>
      <?php get_sidebar(); ?>
    </div>
  <?php endif; ?>

  <main id="main" class="flex-grow grid grid-cols-auto gap-10" tabindex="-1">
    <h1 class="page-title col-span-full"><?php wp_title(''); ?></h1>
    <?php
    set_query_var('header_elem', 'h2');
    while( have_posts() ) {
      the_post();
      get_template_part( 'template-parts/blog-card' );
    }
    ?>

    <!-- pagination -->
    <nav class="col-span-full flex justify-between lg:mx-10 my-4">
      <?php if ( get_previous_posts_link() ): ?>
        <div class="btn-sec-sm"><?php previous_posts_link( __('Newer posts') ); ?></div>
      <?php else: ?>
        <div></div>
      <?php endif; ?>
      <?php if ( get_next_posts_link() ): ?>
        <div class="btn-sec-sm"><?php next_posts_link( __('Older posts') ); ?></div>
      <?php else: ?>
        <div></div>
      <?php endif; ?>
    </nav>
  </main>

  <?php get_template_part( 'template-parts/layout-end' ); ?>

</html>
