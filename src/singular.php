<?php
/**
 * max page size be max-w-screen-2xl
 * content size limited by the template
 * for large screens (e.g. 2.5k) center the max-w-screen-2xl page
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <?php get_template_part( 'template-parts/layout-start' ); ?>

  <?php if (get_post_type() != 'news'): ?>
    <div class="hidden lg:block">
      <?php /* TODO why is this set to page, this is shown on the posts page? */ ?>
      <?php set_query_var('sidebar', 'page'); ?>
      <?php get_sidebar(); ?>
    </div>
  <?php endif; ?>

  <main id="main" class="flex-grow content max-w-prose" tabindex="-1">
    <?php get_template_part( 'template-parts/render_all_posts' ); ?>
  </main>

  <?php get_template_part( 'template-parts/layout-end' ); ?>

</html>

<footer>
  <script>
    addMobileTableLabels ('.wp-block-table')
  </script>
</footer>
