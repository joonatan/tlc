<?php
/**
 * The template for displaying the 404 template in the TLC theme.
 *
 */
 ?>

<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <body>
    <div class="flex justify-center mx-0">
    <div class="max-w-screen-2xl mx-0 lg:mx-2 flex flex-col lg:flex-row flex-grow">

    <div class="hidden lg:block">
      <?php get_sidebar(); ?>
    </div>

    <main id="main" class="flex-grow mx-0 content max-w-screen-md" tabindex="-1">
      <h1 class="text-massive text-bold">404</h1>
      <p><?php _e( "Whoops the page you are looking for isn't here.", 'tlc-custom'); ?></p>
    </main>
  </div>
  </div>

  <?php get_footer(); ?>
  </body>
</html>
