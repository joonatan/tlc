��    +      t  ;   �      �     �     �     �     �     �     �     �                 	        &     ;     S     g     v     �     �     �     �  	   �     �     �     �     �          6     G     V     Z     a     m     �  	   �  /   �  ?   �  E   
  6   P     �     �     �  #   �  r  �     H     ^     k     �     �     �     �     �  	   �     �     �     �     �     	     	     .	     F	     [	     c	     z	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     
     
     
     &
     8
  )   F
  B   p
  I   �
  <   �
     :     H     P     \     $          )                           #      %           (      '                 	          "                                             +                     
                          !   *      &    Accessibility Statement Add New Add New News All All News Archives Blog Blog Sidebar Calendar Edit Edit News Extra Large Template Found %d search results Full Width Template Large Template Main Sidebar More results News No results have been found for No search results for: %s Not Found Not found in Trash Page Page Sidebar Post Type General NameNews Post Type Singular NameNews Previous results Privacy Policy RDI Search Search News Search Results for: %s Update News View News Whoops the page you are looking for isn't here. Widgets in this area will be shown on all pages but front-page. Widgets in this area will be shown on front-page and all other pages. Widgets in this area will be shown only on blog pages. local searchSearch &hellip; news results submit buttonSearch the whole site Project-Id-Version: TLC Custom 1.1
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/custom
PO-Revision-Date: 2022-11-06 23:29+0200
Last-Translator: 
Language-Team: 
Language: fi_FI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.1
X-Domain: tlc-custom
 Saavutettavuusseloste Lisää uusi Lisää uusi uutinen Kaikki Kaikki uutiset Arkisto Blogi Blogi sivupalkki Kalenteri Editoi Editoi uutista Erittäin suuri sivumalli Löytyi %d hakutulosta Täysi leveä sivumalli Suuri sivumalli Ensisijainen sivupalkki Lisää hakutuloksia Uutinen Ei hakutuloksia %s:lle Ei hakutuloksia: %s Ei löytynyt Ei löytynyt roskakorista Sivu Sivupalkki Uutiset Uutinen Edelliset hakutulokset Tietosuojaseloste TKI Hae Etsi uutisista Hakutulokset: %s Päivitä uutista Katso uutinen Hups, etsimääsi sivua ei ole täällä. Tämän alueen widgetit näkyvät kaikilla muilla kuin etusivulla. Tämän alueen widgetit näkyvät etusivulla ja kaikilla muilla sivuilla. Tämän alueen widgetit näkyvät ainoastaan blogi sivuilla. Etsi &hellip; uutinen hakutulosta Etsi koko sivulta 