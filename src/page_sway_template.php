<?php
/*
 * Template Name: Sway template
 * Template Post Type: post, page, news
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <body>
    <main id="main" class="sway-template flex flex-grow content max-w-full relative" tabindex="-1">
      <?php the_post(); ?>
      <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

      <header class="entry-header">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
      </header><!-- .entry-header -->
      <!-- No feature media for sway -->

      <div class="entry-content mx-6">
        <?php the_content(); ?>
      </div><!-- .entry-content -->
    </article><!-- .post -->

    </main>

    <?php get_footer(); ?>
  </body>

</html>

