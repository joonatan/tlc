<?php
/**
 * Sidebar
 *
 * @package TLC
 */

// TODO construct a list of sidebars we need
// homepage => main
// blog / archive => blog
// regular page => [page, main]

$override = $post ? get_post_meta($post->ID, '_page_widget_override', true) : null;

$sn = $override ? $override : get_query_var('sidebar');

// this ends up being a problem on search pages since it retrieves the first post type
// might be a problem for other listing pages too
if (is_search()) {
  // ignore
} elseif (get_post_type() == 'post' || get_post_type() == 'news') {
  $sn = 'blog';
}

$sidebars = [];
switch ($sn) {
  case 'homepage':
    array_push($sidebars, 'sidebar-main');
    break;
  case 'blog':
    array_push($sidebars, 'sidebar-blog');
    break;
  case 'page':
    array_push($sidebars, 'sidebar-page');
    array_push($sidebars, 'sidebar-main');
    break;
  default:
    break;
}

?><!DOCTYPE html>

<?php // Hack to handle left sidebar on all pages except new front-page ?>
<?php if (!is_front_page()) : ?>
  <aside class="min-w-widget max-w-widget text-md py-12 mx-auto">
    <?php get_template_part( 'template-parts/nav-logo' ); ?>
    <hr class="hr-line border border-tuni-violet" />
    <div class="p-4"></div>
<?php else: ?>
  <aside class="min-w-widget max-w-widget text-md pb-8 mx-auto">
<?php endif; ?>
  <?php if ( !empty($sidebars) ): ?>
    <ul id="sidebar">
    <?php foreach( $sidebars as $s ) {
      if (is_active_sidebar($s) ) {
        dynamic_sidebar($s);
      }
    } ?>
    </ul>
  <?php endif; ?>
</aside>

<script>
  fixAccordian();
</script>
