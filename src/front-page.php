<?php
/**
 * Homepage
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <!-- cover image, logo, title text with white background -->
  <div class="relative">
    <!-- WP doesn't crop large images properly when generating small versions.
         This is a work around for handling wide images on mobile without destroying image quality.
    -->
    <div class="block md:hidden">
      <?php the_post_thumbnail('cover-mobile', ['class' => 'cover-media']); ?>
    </div>
    <div class="hidden md:block">
      <?php the_post_thumbnail('full', ['class' => 'cover-media']); ?>
    </div>

    <div class="w-full sm:w-auto absolute top-0 left-0 bg-white/50">
      <div class="relative mx-4">
        <?php get_template_part( 'template-parts/nav-logo' ); ?>
      </div>
    </div>
    <?php // TODO without the feature image the title is broken because of absolute positioning ?>
    <div class="absolute bottom-0 left-0 w-full flex justify-center">
      <div class="bg-white p-8">
        <?php the_title( '<h1 class="text-3xl font-bold text-center text-tuni-violet">', '</h1>' ); ?>
        <div class="text-center">
           <?php echo pll__( get_theme_mod( 'tlc_theme_subheading' ) ); ?>
        </div>
      </div>
    </div>
  </div>

  <?php get_template_part( 'template-parts/layout-start' ); ?>

    <main id="main" class="flex flex-col items-center mx-0 md:mx-4 lg:mx-10 pt-12" tabindex="-1">
      <div class="grid grid-cols-auto-sm gap-10">
        <?php
        // Query both news and blog posts
        // TODO should always include one blog post minimum
        $post_lists = array (
          'news',
          'post',
        );

        $args = array(
          'post_type' => $post_lists,
          'numberposts' => 3,
        );

        $latest_posts = get_posts( $args );

        set_query_var('header_elem', 'h2');
        if ( $latest_posts ) {
          foreach ( $latest_posts as $post ) {
            setup_postdata( $post );
            get_template_part( 'template-parts/blog-card' );
          }
          wp_reset_postdata();
        }
        ?>

        <div class="col-span-full w-full">
        <?php
        wp_nav_menu( array(
          'menu' => 'blog-menu',
          'theme_location' => 'blog-menu',
          'depth' => 1,
          'container' => false,
          'menu_class' => 'w-full flex flex-row justify-center',
          'before' => '<div class="btn-sec-md m-4">',
          'after' => '</div>'
        ));
        ?>
        </div>
      </div>

      <div class="entry-content max-w-prose text-left pt-20">
        <?php the_post(); ?>
        <?php the_content( __( 'Continue reading', 'twentytwenty' ) ); ?>
      </div>

    </main>

    <div class="pt-8 lg:mr-10">
      <?php set_query_var('sidebar', 'homepage'); ?>
      <?php get_sidebar(); ?>
    </div>

  <?php get_template_part( 'template-parts/layout-end' ); ?>

</html>
