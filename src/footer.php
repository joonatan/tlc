<?php
/**
 * Footer for TLC theme
 */

?>
<footer id="site-footer" class="pb-32 pt-8 mt-12 lg:pb-4">
  <a class="fab hidden lg:inline" href="#top" aria-label="<?php _e('Up'); ?>">
     <span class="font-ma-icons text-3xl font-bold">arrow_circle_up</span>
  </a>
  <div class="flex justify-center md:justify-end mb-8">
    <a rel="license" href="http://creativecommons.org/licenses/by-sa/2.0/">
      <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/2.0/88x31.png" />
    </a>
  </div>

  <div class="flex flex-col sm:flex-row justify-between flex-wrap">

    <div class="mx-auto mb-4 sm:mb-0 w-1/2 md:w-auto">
      <div class="mx-auto">
      <?php if(get_privacy_policy_url()): ?>
        <div>
          <a href="<?= get_privacy_policy_url(); ?>">
            <?php _e('Privacy Policy', 'tlc-custom'); ?>
          </a>
        </div>
      <?php endif; ?>
      <?php if ( !empty(tlc_get_accessibility_statement()) ): ?>
        <div>
          <a href="<?php echo get_page_link( tlc_get_accessibility_statement() ); ?>">
            <?php _e('Accessibility Statement', 'tlc-custom'); ?>
          </a>
        </div>
      <?php endif; ?>
      </div>
    </div>

    <div class="mx-auto sm:ml-0 w-1/2 md:w-auto">
      <div class="mx-auto text-center sm:text-right md:text-left">
        <div>
          <a href="mailto:<?php echo get_theme_mod( 'tlc_theme_mailto' ); ?>">
             <?php echo get_theme_mod( 'tlc_theme_mailto' ); ?>
          </a>
        </div>
        <?php if ( !empty(get_theme_mod( 'tlc_theme_hashtag' )) ): ?>
          <div>
             <?php echo get_theme_mod( 'tlc_theme_hashtag' ); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="w-full sm:w-auto md:w-1/4 sm:order-9">
      <a class="" href="<?php bloginfo('url'); ?>">
        <img class="mx-auto md:mr-0 w-auto h-24"
             src="<?php echo get_theme_mod( 'tlc_theme_logo_footer' ); ?>"
             alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) . ' ' . __('Homepage') ); ?>" />
      </a>
    </div>

    <div class="w-full sm:w-1/2 md:w-1/4 order-last sm:order-8 md:order-first">
      <a class="navbar-brand max-w-full" target="_blank" rel="noopener nofollow" href="https://tuni.fi/">
        <img class="mx-auto md:ml-0 w-auto h-24"
             src="<?= tlc_get_parent_logo('', true); ?>"
             alt="<?php _e('Tampere Universities'); ?>">
      </a>
    </div>

  </div>

<script>
  // run on all pages because all the mec widgets have accessibility problems
  uncheckMeny()
</script>

</footer>
<?php wp_footer(); ?>
