<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package TLC
 * @since TLC 0.8
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <div class="flex justify-center mx-0">
  <div class="max-w-screen-2xl mx-0 lg:mx-2 flex flex-col lg:flex-row flex-grow">

    <!-- @todo this needs to check project_type query parameter check, if it's projects then show different sidebar -->
    <div class="hidden lg:block">
      <?php set_query_var('sidebar', 'page'); ?>
      <?php get_sidebar(); ?>
    </div>

    <main id="main" class="flex-grow">
      <?php if ( have_posts() ) : ?>

        <header class="page-header">
          <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'tlc-custom' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        </header><!-- .page-header -->

        <p class="px-2 my-2">
          <?php
            // print how many results we found
            global $wp_query;
            printf( __('Found %d search results', 'tlc-custom'), $wp_query->found_posts );
          ?>
        </p>
        <div class="px-2">
          <?php get_search_form(); ?>
        </div>

        <?php
        while ( have_posts() ) {
          the_post();
          set_query_var('header_elem', 'h2');
          get_template_part( 'template-parts/list-element' );
          set_query_var('header_elem', null);
        }
        ?>

        <!-- pagination -->
        <nav class="flex justify-between lg:mx-10 my-4">
          <?php if ( get_previous_posts_link() ): ?>
            <div class="btn-sec-sm"><?php previous_posts_link( __('Previous results', 'tlc-custom') ); ?></div>
          <?php else: ?>
            <div></div>
          <?php endif; ?>
          <?php if ( get_next_posts_link() ): ?>
            <div class="btn-sec-sm"><?php next_posts_link( __('More results', 'tlc-custom') ); ?></div>
          <?php else: ?>
            <div></div>
          <?php endif; ?>
        </nav>

      <?php else : ?>
        <h1 class="page-title"><?php printf( __( 'No search results for: %s', 'tlc-custom' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        <!-- use the search form that includes query vars -->
        <?php get_search_form(); ?>
      <?php endif; ?>

      <!-- Add button to search without filters
           The better version would be to make to make a single request to all indexed pages
           and then do manual filtering into different categories
           @todo check all filters not only post_type
           @todo add text that tells what the filters where and to clarify
                 that we can show results from the rest of the page
       -->
      <?php if (!empty(get_query_var('post_type')) && get_query_var('post_type') !== 'any' ): ?>
      <div class="text-center">
        <form role="search" method="get" class="flex flex-row justify-around" action="<?php echo esc_url( get_site_url() ); ?>">
          <div class="btn">
            <button type="submit" class="cursor-pointer">
               <span class="font-ma-icons text-xl font-bold align-middle">search</span>
               <?php echo esc_attr_x( 'Search the whole site', 'submit button', 'tlc-custom' ); ?>
            </button>
          </div>
          <input type="hidden" value="<?php echo get_search_query(); ?>" name="s" />
        </form>
      </div>
      <?php endif; ?>
    </main>
  </div>
  </div>

  <?php get_footer(); ?>
</body>
</html>
