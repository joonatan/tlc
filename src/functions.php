<?php
/**
 */

/* Preload Google Fonts */
function tlc_load_fonts() {
  $font = 'Open+Sans';
    ?>
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="anonymous">
<link rel="preload" href="https://fonts.googleapis.com/css?family=<?= $font; ?>" as="fetch" crossorigin="anonymous">
<script type="text/javascript">
!function(e,n,t){"use strict";var o="https://fonts.googleapis.com/css?family=<?= $font; ?>",r="__3perf_googleFontsStylesheet";function c(e){(n.head||n.body).appendChild(e)}function a(){var e=n.createElement("link");e.href=o,e.rel="stylesheet",c(e)}function f(e){if(!n.getElementById(r)){var t=n.createElement("style");t.id=r,c(t)}n.getElementById(r).innerHTML=e}e.FontFace&&e.FontFace.prototype.hasOwnProperty("display")?(t[r]&&f(t[r]),fetch(o).then(function(e){return e.text()}).then(function(e){return e.replace(/@font-face {/g,"@font-face{font-display:swap;")}).then(function(e){return t[r]=e}).then(f).catch(a)):a()}(window,document,localStorage);
</script>
    <?php
}
add_action( 'wp_head', 'tlc_load_fonts' );

function site_scripts() {
  wp_enqueue_style('tlc_style', get_template_directory_uri() . '/style.css', [], '1.5.0');
  wp_enqueue_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', [], false, false );
  wp_enqueue_script('tlc_custom_js', get_template_directory_uri() . '/assets/js/tlc.js', ['jquery']);
}

add_action( 'wp_enqueue_scripts', 'site_scripts' );

// Translations
// load_theme_textdomain requires the filename to be LANG e.g. fi_FI.mo nothing else.
// Also fi locale is 'fi' not 'fi_FI' at least on some computers.
function tlc_load_theme_textdomain() {
  load_theme_textdomain( 'tlc-custom', get_template_directory().'/languages' );
}
add_action( 'after_setup_theme', 'tlc_load_theme_textdomain' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentytwenty_skip_link_focus_fix() {
  // The following is minified via `terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
  ?>
  <script>
  /(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
  </script>
  <?php
}
add_action( 'wp_print_footer_scripts', 'twentytwenty_skip_link_focus_fix' );

function register_my_menus() {
  register_nav_menus(
    array(
      'top-menu' => __( 'Top Menu' ),
      'header-menu' => __( 'Primary Navigation' ),
      'mobile-only-menu' => __( 'Mobile Only Menu' ),
      'blog-menu' => __( 'Blog Menu' )
    )
  );
}

add_action( 'init', 'register_my_menus' );

// no hard-coded title tag
add_theme_support( 'title-tag' );

/**
 * Add a sidebars.
 */
function wpdocs_theme_slug_widgets_init() {
  register_sidebar( array(
      'name'          => __( 'Main Sidebar', 'tlc-custom' ),
      'id'            => 'sidebar-main',
      'description'   => __( 'Widgets in this area will be shown on front-page and all other pages.', 'tlc-custom' ),
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget'  => '</li>',
      'before_title'  => '<h2 class="widgettitle">',
      'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
      'name'          => __( 'Page Sidebar', 'tlc-custom' ),
      'id'            => 'sidebar-page',
      'description'   => __( 'Widgets in this area will be shown on all pages but front-page.', 'tlc-custom' ),
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget'  => '</li>',
      'before_title'  => '<h2 class="widgettitle">',
      'after_title'   => '</h2>',
  ) );
  register_sidebar( array(
      'name'          => __( 'Blog Sidebar', 'tlc-custom' ),
      'id'            => 'sidebar-blog',
      'description'   => __( 'Widgets in this area will be shown only on blog pages.', 'tlc-custom' ),
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget'  => '</li>',
      'before_title'  => '<h2 class="widgettitle">',
      'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );

add_theme_support( 'post-thumbnails' );

function add_thumb_size() {
  add_image_size('list-thumb', 200, 200, true);
  add_image_size('grid-thumb', 370, 0);
  add_image_size('front-page', 1024, 900, true);
  add_image_size('cover-mobile', 768, 525, true);
}
add_action( 'after_setup_theme', 'add_thumb_size' );

/**
 * Customizer options
 * - Logo
 * - Feature image (hero-image)
 * - TODO about page url (where a click to the description lands
 * - TODO link to the parent organisation homepage
 *
 * TODO how to use the rest of the arguments (like img alt text and tooltip)
 */
function your_theme_new_customizer_settings($wp_customize) {
  // Logo
  $wp_customize->add_setting('tlc_theme_logo');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tlc_theme_logo',
    array(
      'label' => 'Upload Logo',
      'section' => 'title_tagline',
      'settings' => 'tlc_theme_logo',
    )
  ));

  $wp_customize->add_setting('tlc_theme_parent_logo');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tlc_theme_parent_logo',
    array(
      'label' => 'Upload Parent Organisation Logo',
      'section' => 'title_tagline',
      'settings' => 'tlc_theme_parent_logo',
    )
  ));
  $wp_customize->add_setting('tlc_theme_parent_logo_en');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tlc_theme_parent_logo_en',
    array(
      'label' => 'Upload Parent Organisation Logo (EN)',
      'section' => 'title_tagline',
      'settings' => 'tlc_theme_parent_logo_en',
    )
  ));

  // ---------------------- Add Front-page Section ---------------------------
  $wp_customize->add_section('tlc_theme_front_page', array(
    'title' => 'Front-page',
    'description' => '',
    'priority' => 21,
  ));

  $wp_customize->add_setting('tlc_theme_subheading');
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tlc_theme_subheading',
    array(
      'label' => 'Subheading',
      'section' => 'tlc_theme_front_page',
      'settings' => 'tlc_theme_subheading',
    )
  ));

  // TODO default values don't work
  // https://wordpress.org/support/topic/defaults-for-customizer-options-not-working/

  // ---------------------- Footer Section -----------------------------------
  $wp_customize->add_section('tlc_theme_footer', array(
    'title' => 'Footer',
    'description' => '',
    'priority' => 30,
  ));

  $wp_customize->add_setting('tlc_theme_logo_footer');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tlc_theme_logo_footer',
    array(
      'label' => 'Logo in Footer',
      'section' => 'tlc_theme_footer',
      'settings' => 'tlc_theme_logo_footer',
    )
  ));

  $wp_customize->add_setting('tlc_theme_parent_logo_footer');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tlc_theme_parent_logo_footer',
    array(
      'label' => 'Parent Organisation Logo in Footer',
      'section' => 'tlc_theme_footer',
      'settings' => 'tlc_theme_parent_logo_footer',
    )
  ));

  $wp_customize->add_setting('tlc_theme_parent_logo_footer_en');
  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'tlc_theme_parent_logo_footer_en',
    array(
      'label' => 'Parent Organisation Logo (EN) in Footer',
      'section' => 'tlc_theme_footer',
      'settings' => 'tlc_theme_parent_logo_footer_en',
    )
  ));


  $wp_customize->add_setting('tlc_theme_hashtag', array('default' => ''));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tlc_theme_hashtag',
    array(
      'label' => 'Hashtag',
      'section' => 'tlc_theme_footer',
      'settings' => 'tlc_theme_hashtag',
    )
  ));

  $wp_customize->add_setting('tlc_theme_mailto', array('default' => ''));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tlc_theme_mailto',
    array(
      'label' => 'mailto address',
      'section' => 'tlc_theme_footer',
      'settings' => 'tlc_theme_mailto',
    )
  ));

  $wp_customize->add_setting('tlc_theme_accessibility_post_id', array('default' => ''));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tlc_theme_accessibility_post_id',
    array(
      'label' => 'Accessibility statement Post ID',
      'section' => 'tlc_theme_footer',
      'settings' => 'tlc_theme_accessibility_post_id',
    )
  ));
}
add_action('customize_register', 'your_theme_new_customizer_settings');

// Add custom metabox for changing page widgets
function tlc_add_widgets_meta_box() {
  $screens = ['page'];
  add_meta_box( 'page_widget_override', __('Widget override', 'tlc-custom'), 'tlc_widgets_meta_box_html', $screens, 'side' );
}
add_action('add_meta_boxes', 'tlc_add_widgets_meta_box');

function tlc_widgets_meta_box_html($post) {
  // current values
  $widget_override = get_post_meta($post->ID, '_page_widget_override', true);

  ?>
  <div class="flex flex-col">
    <div>
      <label for="page_widget_override"><?php _e('Widget override') ?></label>
      <input type="text" name="page_widget_override" id="page_widget_override"
        value="<?php echo $widget_override ?>" />
    </div>
  </div>
  <?php
}

function tlc_save_widgets_postdata($post_id) {
  if (array_key_exists('page_widget_override', $_POST)) {
    update_post_meta( $post_id, '_page_widget_override', $_POST['page_widget_override']);
  }
}
add_action('save_post', 'tlc_save_widgets_postdata');

// Need to flush for taxonomy pages to not return 404
add_action('init', 'custom_taxonomy_flush_rewrite');
function custom_taxonomy_flush_rewrite() {
  global $wp_rewrite;
  $wp_rewrite->flush_rules();
}

// Add a post type filtering using "?=post_type=" http query param
function type_search_filter($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    $post_type = $_GET['post_type'] ?? null;
    if ($post_type && $query->is_search) {
      $query->set('post_type', $post_type);
    }
  }
  return $query;
}
add_filter('pre_get_posts', 'type_search_filter');

function langugage_selector () {
  if (function_exists('pll_the_languages')) {
    pll_the_languages(array("hide_current" => 1));
  }
}

function algolia_search_filter($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $s = $query->query['s'];

      $indexName = 'tlc';
      if (function_exists('pll_current_language')) {
        if ( 'en' === pll_current_language()) {
          $indexName = 'tlc_en';
        }
      }

      $res = algolia_custom_search( $s, $indexName );
      $ids = array_map(function ($x) { return $x['ID']; }, $res['hits']);
      $query->set('post__in', $ids);
      $query->set('s', '');
    }
  }
  return $query;
}

if (function_exists('algolia_custom_search')) {
  add_filter('pre_get_posts', 'algolia_search_filter');
}

// Fix a problem with old PHP version (7.0) on the server
// feature image is added to the excerpt so we remove it here.
add_filter('get_the_excerpt', 'filter_excerpt');

function filter_excerpt($param) {
  $start = strpos($param, '<img');
  $stop = strpos($param, '>', $start);

  if ($stop !== false) {
    return substr($param, $stop+1);
  } else {
    return $param;
  }
}

add_filter( 'the_content', 'filter_add_locks_to_intra_links' );
function filter_add_locks_to_intra_links($content) {
  if (is_singular() && is_main_query()) {
    // find all hrefs
    // if the href is to intra add a lock icon to it
    // TODO need to do the same for moodle links
    $pattern = '/(<a[^>]*?href=".*?intra\.[^>]+?>)([^<]+?)(<\/a>)/';
    $p2 = '/(<a[^>]*?href=".*?wiki.tamk\.[^>]+?>)([^<]+?)(<\/a>)/';
    // TODO need to add aria for the lock
    $lock = '<span class="font-ma-icons text-black">lock</span>';
    $replacement = '${1}${2} ' . $lock . '${3}' ;
    return preg_replace(
      $p2,
      $replacement,
      preg_replace($pattern, $replacement, $content)
    );
  }
  return $content;
}

// --------------------------- Add News post type ----------------------------
function tlc_news_post_type() {

  $labels = array(
    'name'                => _x( 'News', 'Post Type General Name', 'tlc-custom' ),
    'singular_name'       => _x( 'News', 'Post Type Singular Name', 'tlc-custom' ),
    'menu_name'           => __( 'News', 'tlc-custom' ),
    'parent_item_colon'   => __( 'Parent News', 'tlc-custom' ),
    'all_items'           => __( 'All News', 'tlc-custom' ),
    'view_item'           => __( 'View News', 'tlc-custom' ),
    'add_new_item'        => __( 'Add New News', 'tlc-custom' ),
    'add_new'             => __( 'Add New', 'tlc-custom' ),
    'edit_item'           => __( 'Edit News', 'tlc-custom' ),
    'update_item'         => __( 'Update News', 'tlc-custom' ),
    'search_items'        => __( 'Search News', 'tlc-custom' ),
    'not_found'           => __( 'Not Found', 'tlc-custom' ),
    'not_found_in_trash'  => __( 'Not found in Trash', 'tlc-custom' ),
  );

  // Set other options for Custom Post Type
  $args = array(
    'label'               => __( 'news', 'tlc-custom' ),
    'description'         => __( 'News', 'tlc-custom' ),
    'labels'              => $labels,
    // Features this CPT supports in Post Editor
    'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'show_in_rest'        => true,
  );

  register_post_type( 'news', $args );
}
add_action( 'init', 'tlc_news_post_type', 0 );

function tlc_get_accessibility_statement() {
  $id = get_theme_mod( 'tlc_theme_accessibility_post_id' );
  if ( empty($id) ) {
    error_log('WARNING: tlc_theme_accessibility_post_id is not defined');
    return '';
  }

  if ( function_exists('pll_get_post') ) {
    return pll_get_post($id);
  } else {
    return get_post($id);
  }
}

function tlc_count_posts($type, $lang = '') {
  if ( function_exists('pll_count_posts') && function_exists('pll_current_language') ) {
    $lo = empty($lang) ? pll_current_language() : $lang;
    $args = array( 'post_type' => $type );
    return pll_count_posts($lo, $args);
  } else {
    return wp_count_posts($type);
  }
}

function tlc_get_parent_logo($lang = '', $footer = false) {
  if ( empty($lang) && function_exists('pll_current_language') ) {
    $lang = pll_current_language();
  }

  if ('en' === $lang && $footer) {
    return get_theme_mod( 'tlc_theme_parent_logo_footer_en' );
  } elseif ('en' === $lang && !$footer) {
    return get_theme_mod( 'tlc_theme_parent_logo_en' );
  } elseif ($footer) {
    return get_theme_mod( 'tlc_theme_parent_logo_footer' );
  } else {
    return get_theme_mod( 'tlc_theme_parent_logo' );
  }
}

function add_pll_strings() {
  if ( function_exists('pll_register_string') ) {
    pll_register_string( 'tlc_theme_subheading', get_theme_mod( 'tlc_theme_subheading' ), 'TLC theme' );
  }
}
add_action( 'init', 'add_pll_strings', 0 );

// OVerride H5P styles
function tlc_alter_h5p_styles (&$styles, $libraries, $embed_type) {
  $styles[] = (object) array(
    'path' => get_template_directory_uri() . '/css/custom-h5p.css',
    'version' => '?ver=0.1'
  );
}
add_action('h5p_alter_library_styles', 'tlc_alter_h5p_styles', 10, 3);

function register_custom_bellows_skins(){
  $base_url_path = get_stylesheet_directory_uri() . '/css/';
  bellows_register_skin( 'tlc-skin' , 'TLC Bellows Skin' , $base_url_path . 'tlc-bellows-skin.css' );
}
add_action( 'init' , 'register_custom_bellows_skins' , 10 );

// Admin CSS
function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

/**
 * WPForms Turn off the email suggestion.
 */
add_filter( 'wpforms_mailcheck_enabled', '__return_false' );

/* --------------------------- Algolia search *******************************/
function get_algolia_index_local () {
  // TODO add default values with error
  if (function_exists('pll_current_language')) {
    if (pll_current_language() === 'en') {
      return get_option('algolia_custom_index_en');
    }
  }
  return get_option('algolia_custom_index');
}

function algolia_load_assets() {
  $clientPath = '/assets/js/vendor/algoliasearch-lite.umd.js';
  $instantSearchPath = '/assets/js/vendor/instantsearch.production.min.js';

  // Create a version number based on the last time the file was modified
  $clientVersion = date("ymd-Gis", filemtime( get_template_directory() . $clientPath));
  $instantSearchVersion = date("ymd-Gis", filemtime( get_template_directory() . $instantSearchPath));

  wp_enqueue_script('algolia-client', get_template_directory_uri() . $clientPath, array(), $clientVersion, true);
  wp_enqueue_script('algolia-instant-search', get_template_directory_uri() . $instantSearchPath, array('algolia-client'), $instantSearchVersion, true);
  wp_enqueue_style('algolia-theme', get_template_directory_uri() . '/css/vendor/satellite-min.css');

  // our custom script
  $algoliaPath = '/assets/js/algolia-search.js';
  $algoliaVersion = date("ymd-Gis", filemtime(get_template_directory() . $algoliaPath));
  wp_register_script('algolia-search', get_template_directory_uri() . $algoliaPath, array('algolia-instant-search'), $algoliaVersion, true);

  wp_localize_script('algolia-search', 'options', array(
    'API_KEY' => get_option('algolia_custom_key_search'),
    'INDEX_NAME' => get_algolia_index_local(),
    'NAME' => "BAU5PWON46",
    'LANG' => function_exists('pll_current_language') ?  pll_current_language() : 'en',
    'NO_RESULTS_STR' => __('No results have been found for', 'tlc-custom'),
    'RESULTS_STR' => __('results', 'tlc-custom'),
    'SEARCHBOX_PLACEHOLDER' => __('Search', 'tlc-custom')
  ));
  wp_enqueue_script('algolia-search');
}
add_action('wp_enqueue_scripts', 'algolia_load_assets');

/**
 * Code to remove json+ld data
 * Can't get the more granual hooks to remove only the author so removing the whole thing
 */
add_action( 'rank_math/head', function() {
	global $wp_filter;
	if ( isset( $wp_filter["rank_math/json_ld"] ) ) {
		unset( $wp_filter["rank_math/json_ld"] );
	}
});

// Remove author name and time to read from meta
// For some reason these are called slack not twitter
add_filter( "rank_math/opengraph/slack/twitter_label1", '__return_false' );
add_filter( "rank_math/opengraph/slack/twitter_data1", '__return_false' );
add_filter( "rank_math/opengraph/slack/twitter_label2", '__return_false' );
add_filter( "rank_math/opengraph/slack/twitter_data2", '__return_false' );

