// Keep the accordian open to the right page after a new request.
// Also Open submenu if the parent page is selected.
// Works for Bellows Accordian WP plugin
const fixAccordian = () => {
  const links = document.querySelectorAll( "a.bellows-target" );
  const uri = window.location.href;
  // remove anchor links
  const furi = uri.replace(/#\w+$/g, '')
  const nodes = Array.from(links).filter((x) => { return x.href === furi; } )

  const openParent = (elem) => {
    // ul elements need to be opened
    // li elements need to be colored (the li -> a elements in them)
    if (elem.nodeName.toLowerCase() === 'ul') {
      elem.style.display = 'block';
    }
    else if (elem.nodeName.toLowerCase() === 'li') {
      const a = elem.querySelector('a');
      if (a) {
        a.setAttribute('style', 'color: white !important; background: #4e008e !important;');
      }
    }

    const isList = (x) => (
      x.nodeName && (
        x.nodeName.toLowerCase() === 'ul' ||
        x.nodeName.toLowerCase() === 'li'
      )
    )

    // recursion
    const parent = elem.parentElement;
    if (isList(parent) && !parent.classList.contains('bellows-nav')) {
      openParent(parent);
    }
  }

  const openList = (elem) => {
    const parent = elem.parentElement;
    const submenu = parent.querySelector('ul');
    if (submenu) {
      submenu.style.display = 'block';
    }
  }

  // TODO map functions should return lists
  // first one needs to be openend once before recursion
  nodes.map(openList)
  nodes.map(openParent)
}

const hideOverflow = (checkbox) => {
  if (checkbox.checked) {
    document.body.style.overflowY='hidden';
  } else {
    document.body.style.overflowY='auto';
  }
}

/* Accept enter key for checking a checkbox */
function checkOnEnter(elem, event) {
  if (event.keyCode == 13) {
    elem.checked = !elem.checked;
    elem.onchange.apply(elem);
  }
}

// hide mobile menu upon page load
const uncheckMeny = () => {
  const meny = document.getElementById('menyAvPaa');
  if (meny) {
    meny.checked = false;
  }
}

const windowSize = () => {
  var win = window,
      doc = document,
      docElem = doc.documentElement,
      body = doc.getElementsByTagName('body')[0],
      x = win.innerWidth || docElem.clientWidth || body.clientWidth,
      y = win.innerHeight|| docElem.clientHeight|| body.clientHeight;
  return { x: x, y: y }
}

const isMobile = () => windowSize().x < 950

/** @type {(elem: Element) => void} */
const highlightSelected = (elem) => {
  elem.classList.add('bg-white', 'lg:bg-tuni-violet', 'text-tuni-violet', 'lg:text-white', 'border-t-1', 'border-white');
}

/** @type {(elem: Element | null) => void} */
const highlightRecursion = (elem) => {
  if (elem == null) {
    return
  }
  if (elem.parentNode.classList.contains('sub-menu')) {
    const next = elem.parentNode.parentNode
    // Mobile menu: open the submenus
    if (isMobile()) {
      const checkbox = next.querySelector('input')
      checkbox.checked = true
    } else {
      // Desktop highlight the top menu
      highlightSelected(next)
    }
    return highlightRecursion(next)
  }
}

// Add inner labels to table with a selector
// labels are used by the mobile layout
/** @type {(selector: string) => void} */
const addMobileTableLabels = (selector) => {
  const tableEl = document.querySelector(selector);
  if (tableEl) {
    const thEls = tableEl.querySelectorAll('thead th');
    const tdLabels = Array.from(thEls).map(el => el.innerText);
    tableEl.querySelectorAll('tbody tr').forEach( tr => {
      Array.from(tr.children).forEach(
        (td, ndx) =>  td.setAttribute('label', tdLabels[ndx])
      );
    });
  }
}

// NOTE need to hook into resize, nav button click, and nav item hover
// TODO this works but we might want a bit of margin on it
// TODO there is an issue where the menu gets stuck to top or right because of the resize.
// Would have to pre calculate the size of the menu without rendering it and flip to the normal side if there is enough space.
// TODO there is also potential issues if the menu can't be fitted either top or bottom because of the height of the menu.
/** @type {() => void} */
function setMenuDirection() {
  // skip the first menu
  const allSubMenus = document.querySelectorAll('#meny > ul > li > ul.sub-menu > li ul');
  for (let i = 0; i < allSubMenus.length; i++) {
    const menu = allSubMenus[i];
    const rect = menu.getBoundingClientRect();
    if (rect.left < 0) {
      menu.classList.remove('meny-left');
    }
    if (rect.right > windowSize().x) {
      menu.classList.add('meny-left');
    }
    if (rect.bottom > windowSize().y) {
      menu.classList.add('meny-top');
    }
    if (rect.top < 0) {
      menu.classList.remove('meny-top');
    }
  }
}
