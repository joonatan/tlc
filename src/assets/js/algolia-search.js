const searchClient = algoliasearch(options.NAME, options.API_KEY);

const postTypeString = (type, lang) => {
  switch (type) {
    case 'projects':
      return lang === 'fi' ? 'TKI' : 'RDI';
    case 'news':
      return lang === 'fi' ? 'Uutinen' : 'News';
    case 'post':
      return lang === 'fi' ? 'Blogi' : 'Blog';
    case 'mec-events':
      return lang === 'fi' ? 'Tapahtuma' : 'Event';
    case 'page':
      return lang === 'fi' ? 'Sivu' : 'Page';
    default:
      return type;
  }
}

const search = instantsearch({
  indexName: options.INDEX_NAME,
  searchClient,
  searchFunction(helper) {
    // Ensure we only trigger a search when there's a query
    if (helper.state.query) {
      helper.search();
    }
  },
});

search.addWidgets([
  instantsearch.widgets.searchBox({
    container: "#searchbox",
    placeholder: options.SEARCHBOX_PLACEHOLDER,
    queryHook(query, search) {
      // Hide the search box if the query is empty
      const searchElem = document.querySelector('#hits-container')
      const statsElem = document.querySelector('#stats')
      if (query !== '') {
        searchElem.style.display = ''
        statsElem.style.display = ''
        search(query)
      } else {
        searchElem.style.display = 'none'
        statsElem.style.display = 'none'
      }
    },
  }),

  // TODO pagination should be 'show more' not pages
  instantsearch.widgets.pagination({
    container: '#pagination',
    // for mobile can't have more than 1 padding (or need to scale the boxes down)
    padding: 1,
  }),

  instantsearch.widgets.configure({
    hitsPerPage: 6,
    attributesToSnippet: ['post_content'],
  }),

  instantsearch.widgets.stats({
    container: '#stats',
    templates: {
      text: `
        {{ nbHits }} ${options.RESULTS_STR}
      `,
    },
  }),

  instantsearch.widgets.hits({
    container: "#hits",
    templates: {
      item(hit) {
        return `
          <a href="${hit.permalink}">
            <article>
              <strong>
                ${instantsearch.highlight({ attribute: 'post_title', highlightedTagName: 'mark', hit })}
              </strong>
              <p>${instantsearch.snippet({ attribute: 'post_content', highlightedTagName: 'mark', hit })}</p>
              <div class="absolute top-0 right-0 rounded-bl-lg py-2 px-4 bg-tuni-light text-tuni-violet">${postTypeString(hit.post_type, options.LANG)}</div>
            </article>
        `;
      },
      // TODO add suggestions / corrections
      // TODO add keywords / tags / categories / project_type

      empty: `<div>${options.NO_RESULTS_STR} {{ query }}</div>.`
    },
  }),
]);

search.start();
