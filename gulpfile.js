const postcss = require('gulp-postcss');
const gulp = require('gulp');
const { series } = require('gulp');
const copy = require('gulp-copy');

const concat = require('gulp-concat');
const zip = require('gulp-zip');

const uglifycss = require('gulp-uglifycss');

const wpPot = require('gulp-wp-pot');
const run = require('gulp-run');
const rename = require('gulp-rename');

const OUTPUT_DIR = 'build'        // output directory for transpiled code
const SRC_DIR = 'src'             // source code directory
const PKG_NAME = 'tlc-theme'      // tar package name without extension
const POT_NAME = 'tlc-custom'     // pot file name without extension
const TEXT_DOMAIN = 'tlc-custom'  // WP text domain for translation

const package = () => (
  gulp.src(`${OUTPUT_DIR}/**/*`)
      .pipe(zip(`${PKG_NAME}.zip`))
      .pipe(gulp.dest('dist'))
)

/* compiler lcss files to individual css needed for some plugins */
const lcss = () => (
  gulp.src(`${SRC_DIR}/lcss/*.css`)
    .pipe(postcss([
      require('postcss-import'),
      require('tailwindcss/nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(gulp.dest(`${OUTPUT_DIR}/css/`))
)

const css = () => (
  gulp.src(`${SRC_DIR}/*.css`)
    .pipe(postcss([
      require('postcss-import'),
      require('tailwindcss/nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(concat({ path: 'style.css', stat: { mode: 0666 }}))
    .pipe(gulp.dest(OUTPUT_DIR))
)

const copyFlags= () => (
  gulp
    .src([`${SRC_DIR}/polylang/*`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const copyFiles = () => (
  gulp
    .src([`${SRC_DIR}/**/*.php`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const copyLanguages = () => (
  gulp
    .src([`${SRC_DIR}/languages/*.mo`])
    .pipe(copy(`${OUTPUT_DIR}/`, { prefix: 1 }))
)

const copyLangFi = () => (
  gulp
    .src(`${SRC_DIR}/languages/fi_FI.mo`)
    .pipe(rename('languages/fi.mo'))
    .pipe(gulp.dest(`${OUTPUT_DIR}/`))
)

const copyAssets = () => (
  gulp
    .src([`${SRC_DIR}/assets/**/*.*`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const copyAdmin= () => (
  gulp
    .src([`${SRC_DIR}/admin/**/*.*`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const copyCustomCSS= () => (
  gulp
    .src([`${SRC_DIR}/css/**/*.*`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const optimizeCss = () => {
  return gulp
    .src(`${SRC_DIR}/*.css`)
    .pipe(postcss([
      require('postcss-import'),
      require('tailwindcss/nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(concat({ path: 'style.css', stat: { mode: 0666 }}))
    .pipe(uglifycss({ "uglyComments": false }))
    .pipe(gulp.dest(`${OUTPUT_DIR}`))
}

// translations task
const mkpot = () => {
  return gulp
    .src(`${SRC_DIR}/**/*.php`)
    .pipe(wpPot({
        domain: `${TEXT_DOMAIN}`,
        package: 'TLC Custom',
        relativeTo: `${SRC_DIR}`,
    }))
    .pipe(gulp.dest(`${POT_NAME}.pot`))
}

const copyDocker = () => {
  return run('./cp_docker.sh').exec();
}

exports.mkpot = mkpot
exports.package = series(optimizeCss, copyFiles, copyFlags, copyAdmin, copyCustomCSS, copyAssets, copyLanguages, copyLangFi, package)
exports.css = css
exports.default = series(css, lcss, copyFiles, copyFlags, copyAdmin, copyCustomCSS, copyAssets, copyLanguages, copyLangFi, copyDocker)
